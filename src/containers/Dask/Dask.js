import React, {Component} from 'react';
import './Dask.css';
import { connect } from 'react-redux';
import {fetchMessages, fetchDateMessages} from "../../store/actions";


class Desk extends Component {

    state = {
        statusNewMessage: false,
        lastMessageDate: '',
    };

    startInterval = () => {
        setInterval(() => {
            this.props.fetchDateMessages(this.state.lastMessageDate)
        }, 5000);
    };

    componentDidMount() {
        this.props.fetchMessages().then(() => {
            const end = this.props.messages[this.props.messages.length - 1];
            this.setState({lastMessageDate: end.date});
            this.startInterval();
        });
    }

    notice = () => this.state.statusNewMessage ? 'show' : 'hide';

    hideNotice() {
        setTimeout(() => {
            this.setState({statusNewMessage: false})
        }, 1000);
    };

    componentWillReceiveProps(nextProps) {
        const oldLastMessage = this.props.messages[this.props.messages.length - 1];
        const newLastMessage = nextProps.messages[nextProps.messages.length - 1];

        if (
            ((newLastMessage !== undefined ? newLastMessage.author : null) !== this.props.author)
            && ((oldLastMessage !== undefined ? oldLastMessage.date : null) !== newLastMessage.date)) {
            this.setState({statusNewMessage: true});
        }

        this.hideNotice();
    };

    render() {
        return (
            <div id='containerMessage' className="container">
                {this.props.messages.map((message, index) => {
                    return (
                        <div className="message" key={index}>
                            <p className="time">{message.date.slice(11, 20)}</p>
                            <p className="author">{message.author}</p>
                            <p className="said">{message.message}</p>
                        </div>
                    )
                })}
                <p className={this.notice()}>Новое сообщение</p>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        messages: state.messages,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMessages: () => dispatch(fetchMessages()),
        fetchDateMessages: (date) => dispatch(fetchDateMessages(date))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Desk);

import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';

import Desk from './Dask/Dask';
import {sendMessage, clearError} from "../store/actions";

class App extends Component {

    state = {
        author: '',
        message: ''
    };

    getAuthor = (event) => {
        this.setState({author: event.target.value});
        this.props.clearError()
    };

    getMessage = (event) => {
        this.setState({message: event.target.value});
        this.props.clearError()
    };

    onSendHandler = (event) => {
        event.preventDefault();
        this.props.sendMessage(this.state);
        this.setState({author: '', message: ''});
    };

    render() {
        return (
            <div className="App">
                <Desk
                    author={this.state.author}
                    messages={this.state.messages}/>

                {this.props.error ? <div><p>{this.props.error}</p></div> : null}

                <input
                    placeholder="author"
                    onChange={this.getAuthor}
                    value={this.state.author}
                />
                <input
                    placeholder='message'
                    onChange={this.getMessage}
                    value={this.state.message}
                />
                <button
                    onClick={(this.onSendHandler)}
                    className="Button"
                >
                    Send
                </button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        error: state.error
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: (message) => dispatch(sendMessage(message)),
        clearError: () => dispatch(clearError())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);

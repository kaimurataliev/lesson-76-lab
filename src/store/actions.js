import axios from '../axios';

export const FETCH_MESSAGES = "FETCH_MESSAGES";
export const FETCH_REQUEST = "FETCH_REQUEST";
export const FETCH_DATE_MESSAGES = "FETCH_ID_MESSAGES";
export const FETCH_ERROR = "FETCH_ERROR";
export const CLEAR_ERROR = "CLEAR_ERROR";

export const fetchRequest = () => {
    return {type: FETCH_REQUEST}
};

export const clearError = () => {
    return {type: CLEAR_ERROR}
};

export const fetchMessagesSuccess = (data) => {
    return {type: FETCH_MESSAGES, data}
};

export const fetchDateMessagesSuccess = (data) => {
    return {type: FETCH_DATE_MESSAGES, data};
};

export const fetchError = (data) => {
    return {type: FETCH_ERROR, data};
};

export const sendMessage = (message) => {
    return (dispatch) => {
        dispatch(fetchRequest());
        return axios.post('/messages', message).then(
            response => {
                return dispatch(fetchDateMessagesSuccess(response.data))
            }, error => {
                dispatch(fetchError(error.response.data.error));
            }
        );
    }
};


export const fetchMessages = () => {
    return (dispatch) => {
        return axios.get('/messages')
            .then(response => {
                dispatch(fetchMessagesSuccess(response.data));
            })
    }
};

export const fetchDateMessages = (date) => {
    return (dispatch) => {
        return axios.get(`/messages/${date}`)
            .then(response => {
                dispatch(fetchDateMessagesSuccess(response.data));
            })
    }
};
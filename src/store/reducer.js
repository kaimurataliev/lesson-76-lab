import * as actions from './actions';

const initialState = {
    messages: [],
    isLoading: false,
    error: null
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_MESSAGES:
            return {...state, messages: action.data};

        case actions.FETCH_REQUEST:
            return {...state, isLoading: true};

        case actions.FETCH_DATE_MESSAGES:
            return {...state, messages: state.messages.concat(action.data)};

        case actions.FETCH_ERROR:
            return {...state, error: action.data};

        case actions.CLEAR_ERROR:
            return {...state, error: null};

        default:
            return state;
    }
};

export default reducer;